package ca.csfoy.servicesweb.netflix.api.actor;

import java.time.LocalDate;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.graphql.spring.boot.test.GraphQLResponse;
import com.graphql.spring.boot.test.GraphQLTest;
import com.graphql.spring.boot.test.GraphQLTestTemplate;

import ca.csfoy.servicesweb.netflix.api.ActorDto;
import ca.csfoy.servicesweb.netflix.controller.ActorController;

@Tag("Api")
@GraphQLTest
@AutoConfigureMockMvc
public class ActorResourceTest {
	
	private static final String ANY_ID = "8e960e8b-d36b-4656-afb4-ff2da45c8e16";
	private static final String ANY_FIRSTNAME = "Scarlett";
	private static final String ANY_LASTNAME = "Johansson";
	private static final LocalDate ANY_BIRTHDATE = LocalDate.now().minusYears(28);
	private static final String ANY_BIO = "Someeeeeeethingg";
	
	@Autowired
    private GraphQLTestTemplate graphQLTestTemplate;
	
	@MockBean
    ActorController actorResolver;
	
	@Test
	public void get_ifGetSuccessful_thenReturn200Ok() throws Exception {
		ActorDto actor = new ActorDto(ANY_ID,ANY_FIRSTNAME,ANY_LASTNAME,ANY_BIRTHDATE.toString(),ANY_BIO);
		
		Mockito.when(actorResolver.getActor(ANY_ID)).thenReturn(actor);

        GraphQLResponse response = graphQLTestTemplate.postForResource("graphql/getActor.graphql");
        //Assertions.assertEquals("",response.getStatusCode());
        //Assertions.assertEquals(response.get("$.data.getActor.id"),ANY_ID);
        //assertThat(response.get("$.data.getUser.username")).isEqualTo(TEST_USERNAME);
	}
	
	@Test
	public void get_ifActorDoesNotExist_thenReturn404Ok() throws Exception {
	}
}
