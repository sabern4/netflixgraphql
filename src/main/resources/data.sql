insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE, BIO)
values ('8e960e8b-d36b-4656-afb4-ff2da45c8e16', 'Robert', 'Downey Jr.', PARSEDATETIME('1965-04-04', 'yyyy-MM-dd'), 'Something interesting');

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE, BIO)
values ('bb0ca8db-d54d-4cd5-b622-a76c827351bd', 'Gwyneth', 'Paltrow', PARSEDATETIME('1972-09-27', 'yyyy-MM-dd'), 'Something boring');

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE, BIO)
values ('e53b5313-ef0e-4a00-9313-c4f0832c69e3', 'Chris', 'Hemsworth', PARSEDATETIME('1983-08-11', 'yyyy-MM-dd'), 'Something funny');

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE, BIO)
values ('da1dd318-db0a-4393-b5d9-b55d143f4fea', 'Scarlett', 'Johansson', PARSEDATETIME('1983-10-23', 'yyyy-MM-dd'), 'Something sad');

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE, BIO)
values ('50eb391d-f6a7-4615-9f9a-99eeea96070f', 'Mark', 'Ruffalo', PARSEDATETIME('1967-11-22', 'yyyy-MM-dd'), 'Something bad');

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE, BIO)
values ('64330e6c-e973-43ec-8e93-3ea7355e12fb', 'Chris', 'Evans', PARSEDATETIME('1981-06-13', 'yyyy-MM-dd'), 'Something chad');

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('63b5d878-214f-4142-86f2-6d1fd9458629', 'Chris', 'Pratt', PARSEDATETIME('1979-06-21', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('a7f40655-7fd6-49ab-b897-96cda0f0d93b', 'Tom', 'Holland', PARSEDATETIME('1996-06-01', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('e95fb12a-0ca2-4532-90e5-eaa1f3c259b4', 'Samuel L.', 'Jackson', PARSEDATETIME('1948-12-21', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('31cc9613-f18b-4c87-8da1-24edbc3373f8', 'Zoe', 'Saldana', PARSEDATETIME('1983-10-23', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('85c2e323-9083-44e1-986c-a97eef5b5711', 'Karen', 'Gillian', PARSEDATETIME('1983-10-23', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('c0890a4d-c034-489b-b5ea-c0600f5c2b72', 'Bradley', 'Cooper', PARSEDATETIME('1975-01-05', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('fb734a44-5ae7-4aa4-a099-ff6825f61485', 'Vin', 'Diesel', PARSEDATETIME('1967-07-18', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('4f25b6f4-9ab8-4d43-830f-33ecc96f2dd3', 'Dave', 'Bautista', PARSEDATETIME('1969-01-28', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('ff01db30-3c97-4558-b5a1-c08d182feb99', 'Tom', 'Hanks', PARSEDATETIME('1956-07-09', 'yyyy-MM-dd'));

insert into ACTOR_ENTITY (ID, FIRSTNAME, LASTNAME, BIRTHDATE)
values ('d3dbc4c9-8c71-47be-be4d-d050d11ea2f2', 'Tim', 'Allen', PARSEDATETIME('1953-06-13', 'yyyy-MM-dd'));