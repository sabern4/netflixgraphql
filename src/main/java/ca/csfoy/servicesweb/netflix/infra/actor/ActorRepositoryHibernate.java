package ca.csfoy.servicesweb.netflix.infra.actor;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import ca.csfoy.servicesweb.netflix.controller.errors.DuplicateException;
import ca.csfoy.servicesweb.netflix.controller.errors.ObjectNotFoundException;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;

@Repository
public class ActorRepositoryHibernate implements ActorRepository {
	
	private final ActorDao dao;
	private final ActorEntityConverter converter;
	
	public ActorRepositoryHibernate(ActorDao dao,
			ActorEntityConverter converter) {
		this.dao = dao;
		this.converter = converter;
	}

	@Override
	public void create(Actor actor) {
		if (isNotDuplicate(actor)) {
			dao.save(converter.toActorEntity(actor));
		} else {
			throw new DuplicateException(
					"An actor '" + actor.getFirstname() + " " + actor.getLastname() + "' already exists.");
		}		
	}

	@Override
	public void save(Actor actor) {
		if (dao.existsById(actor.getId())) {
			dao.save(converter.toActorEntity(actor));
		} else {
			throw new ObjectNotFoundException(
					"Movie with id (" + actor.getId() + ") does not exist and cannot be modified.");
		}
	}

	@Override
	public void saveAll(Collection<Actor> actors) {
		dao.saveAll(actors.stream().map(actor -> converter.toActorEntity(actor)).collect(Collectors.toList()));
	}

	@Override
	public Actor getBy(String id) {
		Optional<ActorEntity> entity = dao.findById(id);
		if (entity.isPresent()) {
			return converter.fromActorEntity(entity.get());
		} else {
			throw new ObjectNotFoundException("The actor with id (" + id + ") does not exist.");
		}
	}

	@Override
	public Actor getBy(String firstname, String lastname) {		
		return converter.fromActorEntity(dao.findByFirstnameAndLastname(firstname, lastname));
	}

	@Override
	public List<Actor> getAll() {
		return dao.findAll().stream()
				.map(entity -> converter.fromActorEntity(entity))
				.collect(Collectors.toList());
	}

	@Override
	public void delete(String id) {
		if (dao.existsById(id)) {
			dao.deleteById(id);
		} else {
			throw new ObjectNotFoundException("Actor with id (" + id + ") does not exist and cannot be deleted.");
		}
		
	}

	private boolean isNotDuplicate(Actor actor) {
		List<Actor> duplicates = getAll().stream().filter(act -> act.isDuplicate(actor)).collect(Collectors.toList());
		return duplicates.isEmpty();
	}

	@Override
	public Actor save(String id, String bio) {
		if (dao.existsById(id)) {
			Optional<ActorEntity> actor=dao.findById(id);
			ActorEntity entity = actor.get();
			entity.bio=bio;
			dao.save(entity);
			return converter.fromActorEntity(entity);
		} else {
			throw new ObjectNotFoundException(
					"Movie with id (" + id + ") does not exist and cannot be modified.");
		}
	}
}
