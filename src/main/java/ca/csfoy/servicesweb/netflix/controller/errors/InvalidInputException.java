package ca.csfoy.servicesweb.netflix.controller.errors;

public class InvalidInputException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidInputException(String message) {
		super(message);
	}
	
	public InvalidInputException(String message, Throwable t) {
		super(message, t);
	}
}
