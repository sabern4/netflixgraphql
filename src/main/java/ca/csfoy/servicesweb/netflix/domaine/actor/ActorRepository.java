package ca.csfoy.servicesweb.netflix.domaine.actor;

import java.util.Collection;
import java.util.List;

public interface ActorRepository {

	void create(Actor actor);

	void save(Actor actor);
	
	Actor save(String id, String bio);
	
	void saveAll(Collection<Actor> actor);

	Actor getBy(String id);
	
	Actor getBy(String name, String lastname);

	List<Actor> getAll();

	void delete(String id);

}